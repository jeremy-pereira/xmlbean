import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(XmlBeanTests.allTests),
		testCase(XmlObjectTests.allTests),
		testCase(StringConvertibleTests.allTests),
    ]
}
#endif
