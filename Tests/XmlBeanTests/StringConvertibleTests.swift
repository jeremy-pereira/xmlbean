//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 26/11/2019.
//

import XCTest
@testable import XmlBean

final class StringConvertibleTests: XCTestCase
{

	func testPositives()
	{
		runPositiveTest(initialValue: XsdUnsignedInt(1792), expectedString: "1792", compare: ==)
		runPositiveTest(initialValue: 1792, expectedString: "1792", compare: ==)
		runPositiveTest(initialValue: Double(1792), expectedString: "1792.0", compare: ==)
		runPositiveTest(initialValue: "foo", expectedString: "foo", compare: ==)
		runPositiveTest(initialValue: Optional<String>("foo"), expectedString: "foo", compare: ==)
		runPositiveTest(initialValue: XsdInt(1792), expectedString: "1792", compare: ==)
		runPositiveTest(initialValue: true, expectedString: "true", compare: ==)
		runPositiveTest(initialValue: false, expectedString: "false", compare: ==)
		// A final thing for a type that conforms to string protocol
		if let n = String.convertFrom(string: "x1972".dropFirst())
		{
			XCTAssert(n == "1972")
		}
		else
		{
			XCTFail("Failed to convert from substring")
		}

		if Bool.convertFrom(string: "True") != nil
		{
			XCTFail("True is not a valid xsd:boolean")
		}
	}


	func runPositiveTest<T: StringConvertible>(initialValue: T, expectedString: String?, compare: (T, T) -> Bool)
	{
		let string = initialValue.convertedToString
		XCTAssert(string == expectedString, "\(type(of: initialValue)) failed to convert to string correctly")
		if let string = string
		{
			guard let finalValue = T.convertFrom(string: string)
				else { XCTFail("No final value from \(string)") ; return }
			XCTAssert(compare(initialValue, finalValue))
		}
	}

    static var allTests = [
		("testPositives", testPositives),
	]
}
