import XCTest
@testable import XmlBean

final class XmlBeanTests: XCTestCase
{

	static let testXml = """
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    mc:Ignorable="x14ac xr xr2 xr3"
    xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac"
    xmlns:xr="http://schemas.microsoft.com/office/spreadsheetml/2014/revision"
    xmlns:xr2="http://schemas.microsoft.com/office/spreadsheetml/2015/revision2"
    xmlns:xr3="http://schemas.microsoft.com/office/spreadsheetml/2016/revision3"
    xr:uid="{00000000-0001-0000-0200-000000000000}">
    <dimension ref="A1:A3"/>
    <sheetViews>
        <sheetView tabSelected="1" workbookViewId="0">
            <selection activeCell="A3" sqref="A3"/>
        </sheetView>
    </sheetViews>
    <sheetFormatPr baseColWidth="10" defaultColWidth="8.83203125" defaultRowHeight="15"
        x14ac:dyDescent="0.2"/>
    <cols>
        <col min="1" max="1" width="17.83203125" customWidth="1"/>
    </cols>
    <sheetData>
        <row r="1" spans="1:1" x14ac:dyDescent="0.2">
            <c r="A1" s="3" t="s">
                <v>15</v>
            </c>
        </row>
        <row r="3" spans="1:1" x14ac:dyDescent="0.2">
            <c r="A3">
                <f>IF(3&gt;1,2,1)</f>
                <v>2</v>
            </c>
        </row>
    </sheetData>
    <hyperlinks>
        <hyperlink ref="A1" r:id="rId1" location="bar" tooltip="The Google Site"
            xr:uid="{1F1276A7-618C-294A-A9F2-D2862DDD2178}"/>
    </hyperlinks>
    <pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/>
    <pageSetup orientation="portrait" r:id="rId2"/>
</worksheet>
"""


	func testXmlNameFind()
	{
		let xmlDoc = makeDoc(xmlString: XmlBeanTests.testXml)
		let rootElement = xmlDoc.rootElement()!

		let sheetDataName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "sheetData")
		let rowName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "row")

		let unfoundRows = rowName.find(in: rootElement)
		XCTAssert(unfoundRows.count == 0, "Important: XMLElement.element(for name:uri:) now checks all descendants, not immediate children")

		let foundSheetData = sheetDataName.find(in: rootElement)
		XCTAssert(foundSheetData.count == 1, "Wrong element count: \(foundSheetData)")
		if let sheetDataElement = foundSheetData.first
		{
			let foundRows = rowName.find(in: sheetDataElement)
			XCTAssert(foundRows.count == 2, "Wrong element count: \(foundRows)")
		}
	}
	
	func testXmlMatchedChildren()
	{
		let xmlDoc = makeDoc(xmlString: XmlBeanTests.testXml)
		let rootElement = xmlDoc.rootElement()!

		let sheetDataName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "sheetData")
		let rowName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "row")

		let unfoundRows = rowName.matchedChildren(of: rootElement)
		XCTAssert(unfoundRows.count == 0, "Important: XMLElement.element(for name:uri:) now checks all descendants, not immediate children")

		let foundSheetData = sheetDataName.matchedChildren(of: rootElement)
		XCTAssert(foundSheetData.count == 1, "Wrong element count: \(foundSheetData)")
		if let sheetDataElement = foundSheetData.first
		{
			let foundRows = rowName.matchedChildren(of: sheetDataElement)
			XCTAssert(foundRows.count == 2, "Wrong element count: \(foundRows)")
		}
	}

	func testXmlNameMatches()
	{
		let xmlDoc = makeDoc(xmlString: XmlBeanTests.testXml)
		let rootElement = xmlDoc.rootElement()!

		let sheetDataName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "sheetData")
		let rootName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "worksheet")


		XCTAssert(!sheetDataName.matches(node: rootElement), "\(sheetDataName) should not match the root element")
		XCTAssert(rootName.matches(node: rootElement), "\(rootName) should match the root element")
	}


	func testXmlNameUniqueMatchedChildren()
	{
		let xmlDoc = makeDoc(xmlString: XmlBeanTests.testXml)
		let rootElement = xmlDoc.rootElement()!

		let sheetDataName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "sheetData")
		let unknownName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "foo")
		let rowName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "row")

		do
		{
			guard let foundSheetData = try sheetDataName.uniqueMatchedChild(of: rootElement)
			else
			{
				XCTFail("Did not find the sheetData")
				return
			}
			XCTAssert(try unknownName.uniqueMatchedChild(of: foundSheetData) == nil)
			do
			{
				_ = try rowName.uniqueMatchedChild(of: foundSheetData)
				XCTFail("Multiple rows should throw an exception")

			}
			catch XmlError.duplicate
			{
				// What we expected
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testMakeXmlElement()
	{
		let sheetDataName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "sheetData")
		let element = sheetDataName.makeElement(attributes: ["attr" : "foo"])
		XCTAssert(element.localName == "sheetData")
		XCTAssert(element.uri == sheetDataName.namespace)
		XCTAssert(element.attributeString(name: "attr") == "foo")
	}

	func testXmlNameInitFromElement()
	{
		let xmlDoc = makeDoc(xmlString: XmlBeanTests.testXml)
		let rootElement = xmlDoc.rootElement()!

		let rootName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "worksheet")

		let madeName = XmlName(element: rootElement)
		XCTAssert(madeName == rootName)

	}

	func testXmlNameDescription()
	{
		let rootName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main", localName: "worksheet")
		XCTAssert(rootName.description == "worksheet in http://schemas.openxmlformats.org/spreadsheetml/2006/main",
				  "Wrong description '\(rootName)'")
	}

	private func makeDoc(xmlString: String) -> XMLDocument
	{
		let data = xmlString.data(using: .utf8)!
		return try! XMLDocument(data: data, options: [])
	}

    static var allTests = [
        ("testXmlNameFind", testXmlNameFind),
		("testXmlMatchedChildren", testXmlMatchedChildren),
		("testXmlNameMatches", testXmlNameMatches),
		("testXmlNameUniqueMatchedChildren", testXmlNameUniqueMatchedChildren),
		("testMakeXmlElement", testMakeXmlElement),
		("testXmlNameInitFromElement", testXmlNameInitFromElement),
		("testXmlNameDescription", testXmlNameDescription)
    ]
}
