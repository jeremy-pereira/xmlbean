//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 26/11/2019.
//

import XCTest
@testable import XmlBean

final class XmlObjectTests: XCTestCase
{

	private func makeDoc(xmlString: String) -> XMLDocument
	{
		let data = xmlString.data(using: .utf8)!
		return try! XMLDocument(data: data, options: [])
	}

	static let testXml = """
	<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"
		xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
		xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
		mc:Ignorable="x14ac xr xr2 xr3"
		xmlns:x14ac="http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac"
		xmlns:xr="http://schemas.microsoft.com/office/spreadsheetml/2014/revision"
		xmlns:xr2="http://schemas.microsoft.com/office/spreadsheetml/2015/revision2"
		xmlns:xr3="http://schemas.microsoft.com/office/spreadsheetml/2016/revision3"
		xr:uid="{00000000-0001-0000-0200-000000000000}">
		<dimension ref="A1:A3"/>
		<sheetViews>
			<sheetView tabSelected="1" workbookViewId="0">
				<selection activeCell="A3" sqref="A3"/>
			</sheetView>
		</sheetViews>
		<sheetFormatPr baseColWidth="10" defaultColWidth="8.83203125" defaultRowHeight="15"
			x14ac:dyDescent="0.2"/>
		<cols>
			<col min="1" max="1" width="17.83203125" customWidth="1"/>
		</cols>
		<sheetData>
			<row r="1" spans="1:1" x14ac:dyDescent="0.2">
				<c r="A1" s="3" t="s">
					<v>15</v>
				</c>
			</row>
			<row r="3" spans="1:1" x14ac:dyDescent="0.2">
				<c r="A3">
					<f>IF(3&gt;1,2,1)</f>
					<v>2</v>
				</c>
			</row>
		</sheetData>
		<hyperlinks>
			<hyperlink ref="A1" r:id="rId1" location="bar" tooltip="The Google Site"
				xr:uid="{1F1276A7-618C-294A-A9F2-D2862DDD2178}"/>
		</hyperlinks>
		<pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/>
		<pageSetup orientation="portrait" r:id="rId2"/>
	</worksheet>
	"""

	func testAttributeString()
	{
		let dimensionName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main",
									localName: "dimension")
		let doc = makeDoc(xmlString: XmlObjectTests.testXml)
		let rootElement = doc.rootElement()!
		let dimension  = try! dimensionName.uniqueMatchedChild(of: rootElement)!
		XCTAssert(dimension.attributeString(name: "ref") == "A1:A3")
		XCTAssert(dimension.attributeString(name: "foo") == nil)
		XCTAssert(rootElement.attributeString(localName: "uid",
											  uri: "http://schemas.microsoft.com/office/spreadsheetml/2014/revision") == "{00000000-0001-0000-0200-000000000000}")
		XCTAssert(rootElement.attributeString(localName: "uid",
											  uri: "http://schemas.microsoft.com/office/spreadsheetml/2014/revision2") == nil)
	}

	func testConvertibleAttribute()
	{
		let dimensionName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main",
									localName: "dimension")
		let doc = makeDoc(xmlString: XmlObjectTests.testXml)
		let rootElement = doc.rootElement()!
		let dimension  = try! dimensionName.uniqueMatchedChild(of: rootElement)!

		do
		{
			let ref: Ref = try dimension.attribute(name: "ref")
			XCTAssert(ref.start == "A1" && ref.end == "A3")
			let ref2: Ref = try rootElement.attribute(name: "ref", default: Ref(start: "B1", end: "B10"))
			XCTAssert(ref2.start == "B1" && ref2.end == "B10")
			do
			{
				let foo: Ref = try rootElement.attribute(name: "ref")
				XCTFail("Should not have a foo here: \(foo.start)")
			}
			catch {}
			do
			{
				let foo: Ref = try rootElement.attribute(name: "ref")
				XCTFail("Should not have a foo here: \(foo.start)")
			}
			catch XmlError.noDefaultAttributeValue {}
			do
			{
				let foo: Ref = try rootElement.attribute(name: "xr:uid")
				XCTFail("Should not have a foo here: \(foo.start)")
			}
			catch XmlError.typeMismatch {}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testMakeObject()
	{
		let dimensionName = XmlName(namespace: "http://schemas.openxmlformats.org/spreadsheetml/2006/main",
									localName: "dimension")
		let doc = makeDoc(xmlString: XmlObjectTests.testXml)
		let rootElement = doc.rootElement()!
		let dimensionElement  = try! dimensionName.uniqueMatchedChild(of: rootElement)!
		do
		{
			let dimension: Dimension = try dimensionElement.makeObject()
			XCTAssert(dimension.ref.start == "A1")
			XCTAssert(dimension.xmlText == "stub text")
		}
		catch
		{
			XCTFail("\(error)")
		}

	}

	func testPrefixedName()
	{
		let doc = makeDoc(xmlString: XmlObjectTests.testXml)
		let rootElement = doc.rootElement()!
		let prefixedName = rootElement.prefixedName(namespace: "http://schemas.microsoft.com/office/spreadsheetml/2014/revision",
													localName: "uid")
		XCTAssert(prefixedName == "xr:uid")
		let prefixedName2 = rootElement.prefixedName(namespace: "http://schemas.microsoft.com/office/spreadsheetml/2014/revisionX",
													localName: "uid")
		XCTAssert(prefixedName2 == "uid")
	}

    static var allTests = [
		("testAttributeString", testAttributeString),
		("testConvertibleAttribute", testConvertibleAttribute),
		("testPrefixedName", testPrefixedName),
		("testMakeObject", testMakeObject),
	]
}

fileprivate struct Ref
{
	let start: String
	let end: String

	init(start:String, end: String)
	{
		self.start = start
		self.end = end
	}
}

extension Ref: StringConvertible
{
	static func convertFrom<T>(string: T) -> Ref? where T : StringProtocol
	{
		let components = string.split(separator: ":")
		guard components.count == 2 else { return nil }
		return Ref(start: String(components[0]), end: String(components[1]))
	}

	var convertedToString: String?
	{
		return "\(start):\(end)"
	}
}

fileprivate struct Dimension: XmlObject
{

	func xmlText(options: Options) -> String
	{
		return "stub text"
	}

	let ref: Ref
	let element: XMLElement

	init(element: XMLElement) throws
	{
		ref = try element.attribute(name: "ref")
		self.element = element
	}

	func makeElement(name: XmlName) throws -> XMLElement
	{
		return element
	}
}
