import XCTest

import XmlBeanTests

var tests = [XCTestCaseEntry]()
tests += XmlBeanTests.allTests()
XCTMain(tests)
