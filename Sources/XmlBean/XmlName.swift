//
//  XmlName.swift
//  
//
//  Created by Jeremy Pereira on 18/11/2019.
//

import Foundation

/// Models a node name with a namespace
public struct XmlName: Hashable
{
	/// The namespace of the name
	public let namespace: String?
	/// The local name of an element
	public let localName: String


	/// Initialise this name
	///
	/// - Parameter namespace: The namespace of this name - defaults to no namespace
	/// - Parameter localName: The local name of the name
	public init(namespace: String? = nil, localName: String)
	{
		self.namespace = namespace
		self.localName = localName
	}


	/// Initialise a name that matches the given elment.
	/// - Parameter element: The element to initialise the name from
	public init(element: XMLElement)
	{
		self.namespace = element.uri
		guard let localName = element.localName
			else { fatalError("Wrong assumption that an element must have a local name") }
		self.localName = localName
	}


	/// Tests whether the name of the node matches this name
	///
	/// - Parameter node: The node to check
	/// - Returns: `true` if the nodes local name and namespace match
	public func matches(node: XMLNode) -> Bool
	{
		return node.kind == .element && node.localName ?? "" == localName && node.uri == namespace
	}


	/// All the children of the given node that match this name
	///
	/// If the node is not an element we assume it has no children.
	///
	/// - Parameter of: node Node to match the children of
	/// - Returns: an array of the children elements that match this name
	public func matchedChildren(of node: XMLNode) -> [XMLElement]
	{
		guard let element = node as? XMLElement else { return [] }
		return find(in: element)
	}

	/// Find a unique child of the given element
	/// - Parameter node: The parent node
	/// - Returns: The child node if it exists or `nil` if it doesn't.
	/// - Throws: If there is more than one child that matches the name.
	public func uniqueMatchedChild(of node: XMLNode) throws -> XMLElement?
	{
		let elements = matchedChildren(of: node)
		guard elements.count < 2 else { throw XmlError.duplicate(parent: node.xPath ?? "?", childName: self) }
		return elements.first
	}

	/// Finds elements in the children under the given element that match this
	/// name.
	/// - Parameter element: Element to search under
	/// - Returns: An array of the matching elements
	public func find(in element: XMLElement) -> [XMLElement]
	{
		return element.elements(forLocalName: localName, uri: namespace)
	}

	/// Creates a new XML element
	///
	/// The new element is guaranteed to match  this name
	/// - Parameter attributes: A dictionary of attributes to add to the new
	///                         element. If attributes are in a different
	///                         namespace to the element, they should be added
	///                         after. This defaults to no attributes
	/// - Returns: A new XML element matching this name
	public func makeElement(attributes: [String : String] = [:]) -> XMLElement
	{
		let ret = XMLElement(name: localName, uri: namespace)
		for (name, value) in attributes
		{
			let attributeNode = XMLNode.attribute(withName: name, stringValue: value) as! XMLNode
			ret.addAttribute(attributeNode)
		}
		return ret
	}
}

extension XmlName: CustomStringConvertible
{
	public var description: String { "\(localName) in \(namespace ?? "no namespace")" }
}

