//
//  File.swift
//  
//
//  Created by Jeremy Pereira on 18/11/2019.
//


/// Errors that might be thrown parsing XML or writing it.
public enum XmlError: Error
{
	case noDefaultAttributeValue(path: String, attribute: String)
	case typeMismatch(path: String, type: String, value: String)
	case duplicate(parent: String, childName: XmlName)
}
