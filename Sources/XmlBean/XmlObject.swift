//
//  XmlObject.swift
//  
//
//  Created by Jeremy Pereira on 18/11/2019.
//
// Main protocols and extensions thereof

import Foundation


/// An object that models an XML element
public protocol XmlObject
{
	init(element: XMLElement) throws

	/// Returns the object serialised as a string as it would appear in an XML
	/// document.
	///
	/// - Parameter options: Xml options governing the display of the string
	/// - Returns: The element as a string
	func xmlText(options: Options) -> String

	/// Make a new XML element from the object
	///
	///
	/// - Parameter name: The name that will be given to the element.
	/// - Returns: a new XML element that camn be inserted into a document
	/// - Throws: An object may throw if it cannot create the XML for any
	///           reason.
	func makeElement(name: XmlName) throws -> XMLElement
}

public extension XmlObject
{
	/// The object as a string as it would appear in an XML document
	///
	/// This is the same as `xmlString(options: [])`
	var xmlText: String { return xmlText(options: XMLNode.Options() ) }

}

/// This protocol allows you to have a set of options based on youre own type
/// and a translation to `XMLNode.Options`
public protocol Options
{
	var foundationOptions: XMLNode.Options { get }
}

extension XMLNode.Options: Options
{
	public var foundationOptions: XMLNode.Options { self }
}

public extension XMLElement
{
	func attributeString(localName: String, uri: String) -> String?
	{
		if let node = self.attribute(forLocalName: localName, uri: uri)
		{
			return node.stringValue
		}
		else
		{
			return nil
		}
	}


	/// Gets the attribute for the name ignoring name spaces
	/// - Parameter name: Name of attribute
	func attributeString(name: String) -> String?
	{
		if let node = self.attribute(forName: name)
		{
			return node.stringValue
		}
		else
		{
			return nil
		}
	}

	/// Returns the attribute as a string convertible type
	///
	/// If the attribute isn't there and you don't supply a default, the
	/// function will throw.
	///
	/// If you have an optional attribute, you can prevent this function from
	/// throwing by using a nested optional for the default e.g.
	/// ```
	/// let s: String? = try element.attribute(name: "optionalAttr", default: Optional<String?>.some(nil))
	/// ```
	/// The above will not throw if `optionalAttr` is missing and it will set
	/// `s` to `nil`.
	///
	/// - Parameter name: The name of the attribute including the namespace
	///                   prefix if necessay.
	/// - Parameter default: Default to use if the attribute can't be found
	///                      default is no default, in which case we throw if
	///                      the attribute is missing
	/// - Returns: the given attribute
	func attribute<T : StringConvertible>(name: String, default: T? = nil) throws -> T
	{
		let ret: T
		if let attributeAsString = attributeString(name: name)
		{
			guard let value = T.convertFrom(string: attributeAsString)
			else
			{
				throw XmlError.typeMismatch(path: self.xPath ?? "?",
											type: "\(T.self)",
											value: attributeAsString)
			}
			ret = value
		}
		else if let def = `default`
		{
			ret = def
		}
		else
		{
			throw XmlError.noDefaultAttributeValue(path: self.xPath ?? "?",
												   attribute: name)
		}
		return ret
	}

	/// Get a prefixed name for an element or attribute
	///
	/// If there is no prefix for the name or if the prefix is the defaut, just
	/// return the supplied `localName`, otherwise returns `prefix:localName`.
	/// - Parameters:
	///   - namespace: The namespace of the prefix
	///   - localName: The local name
	/// - Returns: The prefixed name
	func prefixedName(namespace: String, localName: String) -> String
	{
		if let prefix = self.resolvePrefix(forNamespaceURI: namespace), !prefix.isEmpty
		{
			return prefix + ":" + localName
		}
		else
		{
			return localName
		}
	}

	/// Make an object of a given type from this element
	///
	/// As long as the type conforms to `XmlObject` we can attempt to make it
	/// from this element.
	///
	/// - Returns: A new object made from this element
	/// - Throws: If the element does not represent a valid object of type `T`
	func makeObject<T: XmlObject>() throws -> T
	{
		return try T(element: self)
	}
}

public protocol StringConvertible
{
	/// Create a `Self` from a string
	/// - Parameter string: The string to convert
	/// - Returns: the `Self` represented by the string or `nil` if the
	///            conversion fails
	static func convertFrom<T: StringProtocol>(string: T) -> Self?

	/// This object converted to a string
	///
	/// It is possible that you don't want the attribute to appear for some
	/// values. In this case, `convertedToString` should be `nil`
	var convertedToString: String? { get }
}

extension Int: StringConvertible
{
	public static func convertFrom<T>(string: T) -> Int?
		where T : StringProtocol
	{
		return Int(string)
	}

	public var convertedToString: String?
	{
		return self.description
	}
}

/// An XML schema compatible unsigned int
public typealias XsdUnsignedInt = UInt32
/// An XML schema compatible signed int
public typealias XsdInt = Int32

extension XsdUnsignedInt: StringConvertible
{
	public static func convertFrom<T>(string: T) -> XsdUnsignedInt?
		where T : StringProtocol
	{
		return XsdUnsignedInt(string)
	}

	public var convertedToString: String?
	{
		return self.description
	}
}

extension XsdInt: StringConvertible
{
	public static func convertFrom<T>(string: T) -> XsdInt?
		where T : StringProtocol
	{
		return XsdInt(string)
	}

	public var convertedToString: String?
	{
		return self.description
	}
}

extension Double: StringConvertible
{
	public static func convertFrom<T>(string: T) -> Double?
		where T : StringProtocol
	{
		return Double(string)
	}

	public var convertedToString: String?
	{
		return "\(self)"
	}
}

extension String: StringConvertible
{
	public static func convertFrom<T>(string: T) -> String?
		where T : StringProtocol
	{
		if let ret = string as? String
		{
			return ret
		}
		else
		{
			return String(string)
		}
	}
	public var convertedToString: String? { self }
}

extension Bool: StringConvertible
{
	public static func convertFrom<T>(string: T) -> Bool? where T : StringProtocol
	{
		switch string
		{
		case "true", "1":
			return true
		case "false", "0":
			return false
		default:
			return nil
		}
	}

	public var convertedToString: String?
	{
		return self ? "true" : "false"
	}
}

extension Optional: StringConvertible where Wrapped: StringConvertible
{
	public static func convertFrom<T>(string: T) -> Optional<Wrapped>?
	where T : StringProtocol
	{
		guard let toBeWrapped = Wrapped.convertFrom(string: string) else { return nil }
		return Optional(toBeWrapped)
	}

	public var convertedToString: String?
	{
		switch self
		{
		case .some(let value):
			return value.convertedToString
		case .none:
			return nil
		}
	}

}
